FROM continuumio/miniconda3:4.6.14

RUN mkdir /code

WORKDIR /code

COPY ./conda_environment.yml /code/conda_environment.yml
COPY ./server .

RUN conda env create -f /code/conda_environment.yml && \
    conda clean --all --yes

EXPOSE 8889

RUN echo "source activate final_project" > ~/.bashrc
ENV PATH /opt/conda/envs/final_project/bin:$PATH

CMD ["python", "intellirobe_server.py"]
